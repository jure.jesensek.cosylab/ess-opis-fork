PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;

var value = PVUtil.getLong(pvs[0]);
// ConsoleUtil.writeInfo("conversion: " + value);
var str = PVUtil.getString(pvs[1]);
// ConsoleUtil.writeInfo("str: " + str);
var prec = PVUtil.getString(pvs[2]);
// ConsoleUtil.writeInfo("precision: " + prec);

if (value == 0) {
	pvs[1].write("");
	// widget.setPropertyValue("precision", 0);
	pvs[2].write(0);
} else {
	pvs[1].write("mA");
	// widget.setPropertyValue("precision", 1);
	pvs[2].write(1);
}
