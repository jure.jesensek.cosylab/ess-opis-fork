#Generated from VACUUM_VAC-VVA_VAC-VVG.def at 2020-06-11_16:15:17
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

msg  = ""
code = 0

if PVUtil.getLong(pvs[0]):
    code = PVUtil.getLong(pvs[1])

    msgs = dict({
                 99 : "Valve Not Open",
                 98 : "Bypass Interlock Time-Out",
                 97 : "Open Interlock Active (Valve Closed)",
                 96 : "Valve Opening Prevented by Tripped Interlock",
                 15 : "Open Interlock Active (Valve Open)",
                 14 : "Pressure Interlock No. 1 Bypassed / Overridden",
                 13 : "Hardware Interlock No. 1 Bypassed / Overridden",
                 12 : "Pressure Interlock No. 2 Bypassed / Overridden",
                 11 : "Hardware Interlock No. 2 Bypassed / Overridden",
                 10 : "Hardware Interlock No. 3 Bypassed / Overridden",
                 9 : "Software Interlock Bypassed / Overridden",
                 8 : "Open Interlock Active (Valve Closed)",
                 7 : "Valve Opening Prevented by Tripped Interlock",
                 0 : ""
                })

    try:
        msg = msgs[code]
    except KeyError:
        msg = "Warning Code: " + PVUtil.getString(pvs[1])
        ScriptUtil.getLogger().severe("Unknown warning code {} : {}".format(pvs[1], code))

try:
    pvs[2].setValue(msg)
except:
    if widget.getType() != "action_button":
        widget.setPropertyValue("text", msg)
    widget.setPropertyValue("tooltip", msg)
