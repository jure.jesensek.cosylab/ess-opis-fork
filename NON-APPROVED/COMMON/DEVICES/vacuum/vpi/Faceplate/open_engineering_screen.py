from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil
from org.csstudio.display.builder.runtime.pv import PVFactory
from time import sleep

digitelqpce  = "vac_digitelqpce_vpi.bob"

macros = widget.getEffectiveMacros()
macros.expandValues(macros)
controller = macros.getValue("CONTROLLER")
try:
    pumpPV = PVFactory.getPV("{}:iUITypeR".format(controller))
    pump = pumpPV.read()
    PVFactory.releasePV(pumpPV)
    pump = pump.value
    if "digitelqpc" in pump.lower():
        path = digitelqpce
    else:
        path = digitelqpce
        ScriptUtil.getLogger().severe("Cannot determine controller type: '{}', Falling back to Digitel QPCe faceplate".format(pump))
except Exception as e:
    path = digitelqpce
    ScriptUtil.getLogger().severe(str(e))
    ScriptUtil.getLogger().severe("Falling back to Digitel QPCe faceplate")


#macros.add("DEVICENAME", controller)
macros.add("DEVICENAME", "${vacPREFIX}")
dmacros = dict()
for k in macros.getNames():
    dmacros[k] = macros.getValue(k)

ScriptUtil.openDisplay(widget, path, "STANDALONE", dmacros)
