PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;

var pvStartDQ    = 0;
var pvOverRange  = 0;
var pvUnderRange = 0;

var pvSymbol     = pvs[0];

var colorID = 0;

var debug = widget.getEffectiveMacros().getValue("DEBUG");
if (debug) {
	debug = debug[0];
	switch (debug) {
		case '1':
		case 'Y':
		case 'y':
		case 'T':
		case 't':
			debug = true;
			break;

		default:
			debug = false;
	}
}
else
	debug = false;

if (debug)
	Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();
else {
	Logger = new Object();
	Logger.info = function() {}
	Logger.warning = function() {}
	Logger.severe = function(text) { org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger().severe(text);}
}

function log_pv(pv) {
	Logger.info(pv + ": " + PVUtil.getString(pv));
}

try {
	pvStartDQ    = 1 * PVUtil.getInt(pvs[1]);
	pvOverRange  = 2 * PVUtil.getInt(pvs[2]);
	pvUnderRange = 4 * PVUtil.getInt(pvs[3]);

	var sum      = pvOverRange | pvUnderRange;
	var isValid  = (sum & (sum - 1)) == 0 ? 1 : 0;

	log_pv(pvs[1]);
	log_pv(pvs[2]);
	log_pv(pvs[3]);

	if (isValid == 0) {
		Logger.severe(pvSymbol + ": Invalid combination");
	} else if (pvOverRange) {
		Logger.info(pvSymbol + ": OVER-RANGE");
		colorID = 3;
	} else if (pvUnderRange) {
		Logger.info(pvSymbol + ": UNDER-RANGE");
		colorID = 4;
	} else if (pvStartDQ) {
		Logger.info(pvSymbol + ": STARTED");
		colorID = 1;
	} else {
		Logger.info(pvSymbol + ": OFF");
		colorID = 2;
	}
} catch (err) {
	Logger.severe("NO CONNECTION: " + err);
}


pvSymbol.write(colorID);
