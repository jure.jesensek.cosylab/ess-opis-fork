############################
#                          #
#     Helium Inventory     #
#         (C)2019          #
#  Per Nilsson - ESS Cryo  #
#       Version 0b5        #
#                          #
############################

# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.runtime.pv import PVFactory

#####################
# -=> Functions <=- #
#####################
def getACCPDewarVolume(DewarLevelP):
	level = (DewarLevelP/100)*2517 # Level in mm
	volume = 0
	
	if level < 127:
		# 0 + 1.073 l/mm
		volume = level * 1.073
	elif level < 254:
		# 195.49 + 3.149 l/mm -152.4mm
		volume = 195.49 + ((level-152.4)*3.149)
	elif level < 381:
		# 646.68 + 5.125 l/mm - 279,4mm
		volume = 646.68 + ((level-279.4)*5.125)
	elif level < 508:
		# 1346.22 + 6.936 l/mm - 406.4mm
		volume = 1346.22 + ((level-406.4)*6.936)
	elif level < 635:
		# 2264.124 + 8.120 l/mm - 533,4mm
		volume = 2264.124 + ((level-533.4)*8.120)
	elif level < 762:
		# 3318.493 + 8.778 l/mm - 660,4mm
		volume = 3318.493 + ((level-660.4)*8.778)
	elif level < 889:
		# 4443.781 + 9.050 l/mm - 787,4mm
		volume = 4443.781 + ((level-787.4)*9.050)
	else: # Level above 889 mm -> linear
		# 5364.661 + 8.079 l/mm - 889mm
		volume = 5364.661 + ((level-889)*9.079205) #Get the volume at 889 mm and add the remaining level with fixed l/mm		
	#22187l total volume.
	return volume

###########################
# Get all PHS tank masses #
###########################
PHS = []
for i  in range(1,20):
	PVname = "CrS-PHS:Cryo-D-%02d00:HeliumMass" % (i)
	PHS.append(PVUtil.getDouble(PVFactory.getPV(PVname)))

PHSTotal = sum(PHS)

#ScriptUtil.getLogger().info(str(farmTotal))

HPTanks = []
# Get the HP storage
HPTanks.append(PVUtil.getDouble(PVFactory.getPV("CrS-TICP:Cryo-Virt-MISC2:Misc_007")))
HPTanks.append(PVUtil.getDouble(PVFactory.getPV("CrS-TICP:Cryo-Virt-MISC2:Misc_008")))
HPTanks.append(PVUtil.getDouble(PVFactory.getPV("CrS-TICP:Cryo-Virt-MISC2:Misc_009")))

HPTanksTotal = sum(HPTanks)


###########################################
# TICP Dewar Liquid and vapor calculation #
###########################################

# Dens liquid: 125.9 - (0,2295 * DewPressure)
# Dens vapor: 15.745 + (0,2133 * DewPressure)

# Variables setup
TICPDewarLiqV = 0
TICPDewarLiqM = 0
TICPDewarVapV = 0
TICPDewarVapM = 0

TICPDewarPress = 0
TICPDewarMass = 0

#  -=>Variable assign<=-
TICPDewarPress = PVUtil.getDouble(PVFactory.getPV("CrS-TICP:Cryo-PT-53612:Val")) #Dewar pressure
TICPDewarLiqV = PVUtil.getDouble(PVFactory.getPV("CrS-TICP:Cryo-Virt-MISC1:Misc_019")) # Dewar in liters.
TICPDewarVapV = 5555 - TICPDewarLiqV # Whole dewar volym (5555l) - liquid volume.

# -=>Calculations<=-
TICPDewarLiqM = ((TICPDewarLiqV * (125.9-(0.2295*TICPDewarPress)))/1000)
TICPDewarVapM = ((TICPDewarVapV * (15.745+(0.2133*TICPDewarPress)))/1000)
TICPDewarMass = TICPDewarLiqM + TICPDewarVapM

###########################################
# ACCP Dewar Liquid and vapor calculation #
###########################################
 
# Lengt of super-conduction level probe: 2517mm
# PV for level CrS-ACCP:CRYO-LT-63000:Val (0-100% of 2517mm)
# PV 

# -=> Level to liter calculation <=-
ACCPDewarLevelP = 0
ACCPDewarPress = 0
ACCPDewarLiqV = 0

ACCPDewarPress = (PVUtil.getDouble(PVFactory.getPV("CrS-ACCP:CRYO-PT-33850:Val"))) #Dewar pressure
ACCPDewarLevelP = (PVUtil.getDouble(PVFactory.getPV("CrS-ACCP:CRYO-LT-63000:Val"))) #Dewar level probe.

ACCPDewarLiqV = getACCPDewarVolume(ACCPDewarLevelP)
ACCPDewarVaporV = 22187 - ACCPDewarLiqV

# Lets do the mass calculations
ACCPLiqMass = ((ACCPDewarLiqV * (125.9-(0.2295*ACCPDewarPress)))/1000)
ACCPVapMass = ((ACCPDewarVaporV * (15.745+(0.2133*ACCPDewarPress)))/1000)

#ScriptUtil.getLogger().info("ACCP Dewar pressure:" + str(ACCPDewarPress))
#ScriptUtil.getLogger().info("ACCP Dewar level(%):" + str(ACCPDewarLevelP))
#ScriptUtil.getLogger().info("ACCP Dewar liquid volume:" + str(ACCPDewarLiqV))
#ScriptUtil.getLogger().info("ACCP Dewar vapour volume:" + str(ACCPDewarVaporV))
#ScriptUtil.getLogger().info("ACCP Dewar liquid mass:" + str(ACCPLiqMass))
#ScriptUtil.getLogger().info("ACCP Dewar vapour mass:" + str(ACCPVapMass))


ACCPDewarMass = ACCPLiqMass + ACCPVapMass

TotalHeMass = PHSTotal + HPTanksTotal + TICPDewarMass + ACCPDewarMass

# ----====> DEBUG STUFF <====----
# Print some info into the logger for debugging:
#ScriptUtil.getLogger().info("PHS:" + str(PHSTotal))
#ScriptUtil.getLogger().info("HPTanks:" + str(HPTanksTotal))	
#ScriptUtil.getLogger().info("TICP Dewar mass:" + str(TICPDewarMass))
#ScriptUtil.getLogger().info("ACCP Dewar mass:" + str(ACCPDewarMass))

# Try to write to some PVs
PVUtil.writePV("loc://PHS_TotalMass", PHSTotal, 5000)
PVUtil.writePV("loc://HP_TotalMass", HPTanksTotal, 5000)
PVUtil.writePV("loc://TICP_Dewar_Liq", TICPDewarLiqM, 5000)
PVUtil.writePV("loc://TICP_Dewar_Vap", TICPDewarVapM, 5000)
PVUtil.writePV("loc://TICP_Dewar_Tot", TICPDewarMass, 5000)
PVUtil.writePV("loc://ACCP_Dewar_Liq", ACCPLiqMass, 5000)
PVUtil.writePV("loc://ACCP_Dewar_Vap", ACCPVapMass, 5000)
PVUtil.writePV("loc://ACCP_Dewar_Tot", ACCPDewarMass, 5000)
PVUtil.writePV("loc://TotalMass", TotalHeMass, 5000)


